/////////////////////////////////////Mysql
mysql -uroot -p
	CREATE DATABASE zabbix character set utf8 collate utf8_bin;
	CREATE USER 'zabbixuser'@'localhost' IDENTIFIED BY 'new_password_here';
	GRANT ALL ON zabbix.* TO 'zabbixuser'@'localhost' IDENTIFIED BY 'user_password_here' WITH GRANT OPTION;
	FLUSH PRIVILEGES;
	EXIT;

////////////////////////////////////////

////////////////////////////////////////Postgresql
1.sudo su - postgres
2-psql dbname
\d
psql
CREATE DATABASE myproject;
CREATE USER myprojectuser WITH PASSWORD 'password';
ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
ALTER ROLE myprojectuser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;
1-sudo -i -u postgres
2-psql dbname
\d
SELECT * FROM auth_user;
///////////////////////////////////////

///////////////////////////////////////mongoDB
sudo apt install -y mongodb
sudo systemctl status mongodb
mongo --eval 'db.runCommand({ connectionStatus: 1 })'
sudo systemctl start mongodb
sudo systemctl enable mongodb
sudo ufw allow from your_other_server_ip/32 to any port 27017  
sudo ufw status
sudo nano /etc/mongodb.conf
	...
	logappend=true

	bind_ip = 127.0.0.1,your_server_ip
	#port = 27017

	...
sudo systemctl restart mongodb


mongo
show dbs
use <db name>
show collections;
db.collectionName.find()

use db
db.stats()
db.collection.stats()
db.collection.count()
//////////////////////////////////////////
