$TTL    1d
$ORIGIN school-expo.com.

@       IN      SOA     ns1              server (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;

@               IN              NS              ns1
ns1             IN              A               185.211.58.6
@               IN              NS              ns2
ns2             IN              A               185.211.58.6
@               IN      A       185.211.58.6
www             IN      A       185.211.58.6
test            IN      A       185.211.58.6
;test           IN      CNAME   school-expo.com.
